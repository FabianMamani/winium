import org.openqa.selenium.By;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.openqa.selenium.winium.WiniumDriverCommandExecutor;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class BaseApp {


    public static void main (String[] ar) throws MalformedURLException, InterruptedException {
        DesktopOptions _option = new DesktopOptions();
        _option.setApplicationPath("C:\\Windows\\System32\\calc.exe");

        WiniumDriver _winium = new WiniumDriver(new URL("http://localhost:9999"), _option);
        Thread.sleep(5000);

        _winium.findElement(By.id("num9Button")).click();
        _winium.findElement(By.id("plusButton")).click();
        _winium.findElement(By.id("num3Button")).click();
        _winium.findElement(By.id("equalButton")).click();
        Thread.sleep(2000);
        String ret = _winium.findElement(By.id("CalculatorResults")).getAttribute("Name");
        System.out.println(ret);
    }

}
