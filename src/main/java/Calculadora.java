import org.openqa.selenium.By;
import org.openqa.selenium.winium.WiniumDriver;

import java.net.MalformedURLException;
import java.util.HashMap;

public class Calculadora extends Base {
    public HashMap<String,String> objs;

    public Calculadora(WiniumDriver winium) {
        super(winium);
        init();
    }
    static String pathApp(){
        String path="C:\\Windows\\System32\\calc.exe";
        return path;
    }

    private void init() {
        objs = new HashMap<String, String>();
        objs.put("9","num9Button");
        objs.put("8","num8Button");
        objs.put("7","num7Button");
        objs.put("6","num6Button");
        objs.put("5","num5Button");
        objs.put("4","num4Button");
        objs.put("3","num3Button");
        objs.put("2","num2Button");
        objs.put("1","num1Button");
        objs.put("0","num0Button");
        objs.put("+","plusButton");
        objs.put("=","equalButton");
    }

    public void action(String act)
    {
        _winium.findElement(By.id(objs.get(act))).click();
    }

    public String getResult()
    {
       return  _winium.findElement(By.id("CalculatorResults")).getAttribute("Name");
    }
}
