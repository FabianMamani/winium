import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    static WiniumDriver _winium;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws MalformedURLException, InterruptedException {
        DesktopOptions _option = new DesktopOptions();
        _option.setApplicationPath(Calculadora.pathApp());
         _winium = new WiniumDriver(new URL("http://localhost:9999"), _option);
        Thread.sleep(5000);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){

        _winium.quit();
    }

}
