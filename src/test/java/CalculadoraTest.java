import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CalculadoraTest extends BaseTest {

    @Test
    public void operations() throws InterruptedException {
        Calculadora calc = new Calculadora(_winium);

        calc.action("9");
        calc.action("+");
        calc.action("3");
        calc.action("=");

        assertEquals(calc.getResult(),"Se muestra 12");
        Thread.sleep(3000);
    }
}
